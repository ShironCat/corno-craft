#!/bin/bash

git pull
if [ $(cat script_version.txt) != "1" ]
then
    (
        ./start.sh
    )
else
    is_online=$(cat is_online.txt)
    if [ $is_online == "0" ]
    then
        clear
        echo "##########################"
        echo "#                        #"
        echo "# Inicializando Servidor #"
        echo "#                        #"
        echo "##########################"
        echo $(hostname) > is_online.txt
        git add *
        git commit -m "$(date) - Servidor Online!"
        git push
        sed -i "s/server-ip.*/server-ip=$(hamachi | grep address | cut -d ' ' -f 8)/" server.properties
        java -Xmx1024M -Xms1024M -jar server.jar nogui
        clear
        echo "#############################"
        echo "#                           #"
        echo "# Sincronizando Informações #"
        echo "#                           #"
        echo "#############################"
        echo "0" > is_online.txt
        git add *
        git commit -m "$(date) - Servidor Offline!"
        git push
        clear
        echo "######################"
        echo "#                    #"
        echo "# Servidor Encerrado #"
        echo "#                    #"
        echo "######################"
    else
        clear
        echo "###################################"
        echo "#                                 #"
        echo "# Servidor online em outro local! #"
        echo "# Host: $is_online"
        echo "#                                 #"
        echo "###################################"
    fi
fi