git pull
if ($(Get-Content script_version.txt) -ne "1") {
    powershell {
        ./start.ps1
    }
} else {
    $is_online = Get-Content is_online.txt
    if ($is_online -eq 0) {
        Clear-Host
        Write-Output "##########################"
        Write-Output "#                        #"
        Write-Output "# Inicializando Servidor #"
        Write-Output "#                        #"
        Write-Output "##########################"
        Set-Content is_online.txt -Value $(hostname)
        git add *
        git commit -m "$(Get-Date) - Servidor Online!"
        git push
        sed -i "s/server-ip.*/server-ip=$(C:\Program Files (x86)\LogMeIn Hamachi\x64\hamachi-2.exe --cli | grep address | cut -d ' ' -f 8)/" server.properties
        java -Xmx1024M -Xms1024M -jar server.jar nogui
        Clear-Host
        Write-Output "#############################"
        Write-Output "#                           #"
        Write-Output "# Sincronizando Informações #"
        Write-Output "#                           #"
        Write-Output "#############################"
        Set-Content is_online.txt -Value "0"
        git add *
        git commit -m "$(Get-Date) - Servidor Offline!"
        git push
        Clear-Host
        Write-Output "######################"
        Write-Output "#                    #"
        Write-Output "# Servidor Encerrado #"
        Write-Output "#                    #"
        Write-Output "######################"
    } else {
        Clear-Host
        Write-Output "###################################"
        Write-Output "#                                 #"
        Write-Output "# Servidor online em outro local! #"
        Write-Output "# Host: $is_online"
        Write-Output "#                                 #"
        Write-Output "###################################"
    }
}